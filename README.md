# Book
This is a sample project to compile Markdown into a PDF. see https://filpal.wordpress.com/2021/11/28/publish-a-book-using-gitlab/.

- The book will auto generate index based on file name.
- The book will auto numbering all the images/figures.
